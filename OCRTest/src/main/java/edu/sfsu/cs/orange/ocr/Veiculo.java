/**
 * Created by jos3duardo on 24/06/2018.
 */
package edu.sfsu.cs.orange.ocr;

public class Veiculo {
    String id;
    String placa;
    String ano;
    String modelo;
    String cor;

    @Override
    public String toString() {
        return
                    "Placa = "  + placa + "\t" +
                "\t  Ano = " + ano+ "\t" +
                "\t  Cor = " + cor + "\t" +
                "\t  Modelo = " + modelo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }
}
