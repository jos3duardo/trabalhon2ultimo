package edu.sfsu.cs.orange.ocr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class VeiculosActivity extends Activity {

    EditText edPlaca, edCor, edAno, edModelo;
    ListView listView;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    private List<Veiculo> listVeiculo = new ArrayList<Veiculo>();
    private ArrayAdapter<Veiculo> veiculoArrayAdapter;

    //parte da pesquisa
    List<Veiculo> veiculoList = new ArrayList<Veiculo>();
    ArrayAdapter<Veiculo> arrayAdapter;

    Veiculo veiculoSelecionado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_veiculos);

        edPlaca = findViewById(R.id.edPlaca);
        edAno = findViewById(R.id.edAno);
        edCor = findViewById(R.id.edCor);
        edModelo = findViewById(R.id.edModelo);
        listView = findViewById(R.id.listView);

        inicializarFirebase();

        eventoDatabase();

        eventEdit();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                veiculoSelecionado = (Veiculo) parent.getItemAtPosition(position);
                edPlaca.setText(veiculoSelecionado.getPlaca());
                edAno.setText(veiculoSelecionado.getAno());
                edCor.setText(veiculoSelecionado.getCor());
                edModelo.setText(veiculoSelecionado.getModelo());
            }
        });

    }

    private void eventoDatabase(){
        databaseReference.child("Veiculo").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                listVeiculo.clear();

                for (DataSnapshot objSnapshot: dataSnapshot.getChildren()){
                    Veiculo v = objSnapshot.getValue(Veiculo.class);
                    listVeiculo.add(v);
                }

                veiculoArrayAdapter = new ArrayAdapter<Veiculo>(VeiculosActivity.this,
                        android.R.layout.simple_list_item_1, listVeiculo);
                listView.setAdapter(veiculoArrayAdapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void inicializarFirebase(){
        FirebaseApp.initializeApp(VeiculosActivity.this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        //firebaseDatabase.setPersistenceEnabled(true);
        databaseReference = firebaseDatabase.getReference();
    }

    //abre o menu criado
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if (id == R.id.menu_novo){

                Veiculo v = new Veiculo();
                v.setId(UUID.randomUUID().toString());
                v.setPlaca(edPlaca.getText().toString());
                v.setAno(edAno.getText().toString());
                v.setCor(edCor.getText().toString());
                v.setModelo(edModelo.getText().toString());

            //valida se os campos estao em branco
            if (TextUtils.isEmpty(edPlaca.getText().toString())){
                edPlaca.setError("Campo em branco");
            }

            else{
                databaseReference.child("Veiculo").child(v.getId()).setValue(v);
            }
                limparCampos();

        }

        if(id == R.id.menu_atualizar){
            Veiculo v = new Veiculo();
            v.setId(veiculoSelecionado.getId());
            v.setPlaca(edPlaca.getText().toString());
            v.setAno(edAno.getText().toString());
            v.setCor(edCor.getText().toString());
            v.setModelo(edModelo.getText().toString());

            databaseReference.child("Veiculo").child(v.getId()).setValue(v);
            limparCampos();
        }

        if(id == R.id.menu_apaga){
            Veiculo v = new Veiculo();
            v.setId(veiculoSelecionado.getId());

            databaseReference.child("Veiculo").child(v.getId()).removeValue();

        }

        if(id == R.id.menu_pesquisa){
            Intent intent = new Intent(this, PesquisaActivity.class);
            startActivity(intent);
        }

        if(id == R.id.menu_pesquisa_com_camera){
            Intent intent = new Intent(this, CaptureActivity.class);
            startActivity(intent);
        }
        return true;
    }


    private void limparCampos(){
        edModelo.setText("");
        edCor.setText("");
        edAno.setText("");
        edPlaca.setText("");
    }

    //implementando a pesquisa na hora de cadastrar um novo veiculo
    //para que um mesmo veiculo não seja cadastrado varias vezes
    private void eventEdit(){

        edPlaca.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String palavra = edPlaca.getText().toString().trim();
                pesquisarPalavra(palavra);
            }
        });
    }

    private void  pesquisarPalavra(String palavra){
        Query query;

        if (palavra.equals("")){
            query = databaseReference.child("Veiculo").orderByChild("placa");
        }
        else{
            query = databaseReference.child("Veiculo").orderByChild("placa").startAt(palavra).endAt(palavra+"\uf8ff");
        }

        veiculoList.clear();

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //limpa a lista depois da pesquisa e alteração do veiculo
                // caso ela ja esteja cadastrado
                veiculoList.clear();
                for (DataSnapshot objSnapshot: dataSnapshot.getChildren()){
                    Veiculo v = objSnapshot.getValue(Veiculo.class);
                    veiculoList.add(v);
                }

                arrayAdapter = new ArrayAdapter<Veiculo>(VeiculosActivity.this,
                        android.R.layout.simple_list_item_1, veiculoList);
                listView.setAdapter(arrayAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    protected void onResume(){
        super.onResume();
        pesquisarPalavra("");
    }


}
