package edu.sfsu.cs.orange.ocr;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class PesquisaActivity extends Activity {

    EditText edPesquisa;
    ListView listView;

    Veiculo veiculoSelecionado;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

     List<Veiculo> veiculoList = new ArrayList<Veiculo>();
     ArrayAdapter<Veiculo> arrayAdapter;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesquisa);

        listView = findViewById(R.id.ListViewPesquisa);
        edPesquisa = findViewById(R.id.edPesquisas);

        inicializarFirebase();

        eventEdit();


        Intent intent = getIntent();

        String placa = (String) intent.getSerializableExtra("edPlaca");

        edPesquisa.setText(placa);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                veiculoSelecionado = (Veiculo) parent.getItemAtPosition(position);

                veiculoSelecionado = (Veiculo) parent.getItemAtPosition(position);

                Intent intent = new Intent(PesquisaActivity.this, EditiVeiculoctivity.class);

                intent.putExtra("edId", veiculoSelecionado.getId());
                intent.putExtra("edPlaca", veiculoSelecionado.getPlaca());
                intent.putExtra("edAno", veiculoSelecionado.getAno());
                intent.putExtra("edModelo", veiculoSelecionado.getModelo());
                intent.putExtra("edCor", veiculoSelecionado.getCor());
                startActivity(intent);

            }
        });



    }

    private void inicializarFirebase(){
        FirebaseApp.initializeApp(PesquisaActivity.this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }


    private void eventEdit(){
        edPesquisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String palavra = edPesquisa.getText().toString().trim();
                pesquisarPalavra(palavra);
            }
        });
    }

    private void  pesquisarPalavra(String palavra){
        Query query;
        if (palavra.equals("")){
            query = databaseReference.child("Veiculo").orderByChild("placa");
        }
        else{
            query = databaseReference.child("Veiculo").orderByChild("placa").startAt(palavra).endAt(palavra+"\uf8ff");
        }

        veiculoList.clear();

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot objSnapshot: dataSnapshot.getChildren()){
                    Veiculo v = objSnapshot.getValue(Veiculo.class);
                    veiculoList.add(v);
                }

                arrayAdapter = new ArrayAdapter<Veiculo>(PesquisaActivity.this,
                        android.R.layout.simple_list_item_1, veiculoList);
                listView.setAdapter(arrayAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

//    protected void onResume(){
//        super.onResume();
//        pesquisarPalavra("");
//    }

    //abre o menu criado
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();


        if(id == R.id.menu_principal){
            Intent intent = new Intent(this, VeiculosActivity.class);
            startActivity(intent);
        }

        if(id == R.id.menu_pesquisa_com_camera){
            Intent intent = new Intent(this, CaptureActivity.class);
            startActivity(intent);
        }


        return true;
    }



}
