package edu.sfsu.cs.orange.ocr;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class EditiVeiculoctivity extends Activity {


    EditText edPlaca, edCor, edAno, edModelo, edId;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editi_veiculoctivity);

        edPlaca = findViewById(R.id.edPlaca);
        edAno = findViewById(R.id.edAno);
        edCor = findViewById(R.id.edCor);
        edModelo = findViewById(R.id.edModelo);
        edId = findViewById(R.id.edId);


        Intent intent = getIntent();

        String idV = (String) intent.getSerializableExtra("edId");
        String placa = (String) intent.getSerializableExtra("edPlaca");
        String ano = (String) intent.getSerializableExtra("edAno");
        String modelo = (String) intent.getSerializableExtra("edModelo");
        String cor = (String) intent.getSerializableExtra("edCor");

        edId.setText(idV);
        edPlaca.setText(placa);
        edAno.setText(ano);
        edCor.setText(cor);
        edModelo.setText(modelo);

        OcrResult ocr = new OcrResult();
        CaptureActivity captureActivity = new CaptureActivity();




    }



    //abre o menu criado
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.menu_editar){

            Veiculo v = new Veiculo();

            v.setId(edId.getText().toString());
            v.setPlaca(edPlaca.getText().toString());
            v.setAno(edAno.getText().toString());
            v.setCor(edCor.getText().toString());
            v.setModelo(edModelo.getText().toString());

            databaseReference.child("Veiculo").child(v.getId()).setValue(v);

            Intent intent = new Intent(this, VeiculosActivity.class);
            startActivity(intent);

        }

        if(id == R.id.menu_volta){
            Intent intent = new Intent(this, PesquisaActivity.class);
            startActivity(intent);
        }


        return true;
    }
}
